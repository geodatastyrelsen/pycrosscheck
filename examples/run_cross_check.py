import logging

from pycrosscheck.common.logging.logging import LoggingSetup
from pycrosscheck.cross_check import CrossCheck

LoggingSetup.set_logging(ns_list=["pycrosscheck"])
logger = logging.getLogger(__name__)

# edit to a valid SEM path
sem_path = r"C:\_work\semantics\test\fau\test_1m.sem"
logger.debug('input: %s' % sem_path)

only_extract = False

cc = CrossCheck(path=sem_path, only_extract=only_extract)
cc.run()
cc.open_output_folder()
