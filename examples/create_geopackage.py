import os
from osgeo import ogr
from shapely.geometry import LineString

from pycrosscheck.common.formats.gdal_aux import GdalAux
from pycrosscheck.common.testing.testing import Testing

x_min = 478763.29
x_max = 478901.43
y_min = 6347605.02
y_max = 6347764.37

testing = Testing(root_folder=os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
gpkg_path = os.path.join(testing.output_data_folder(), "bbox.gpkg")

GdalAux()
driver = ogr.GetDriverByName('GPKG')
ds = driver.CreateDataSource(gpkg_path)
layer = ds.CreateLayer('PySemantics', None, ogr.wkbLineString)
defn = layer.GetLayerDefn()

feat = ogr.Feature(defn)
geom = ogr.CreateGeometryFromWkb(
    LineString([(x_min, y_min), (x_min, y_max), (x_max, y_max), (x_max, y_min), (x_min, y_min)]).wkb)
feat.SetGeometry(geom)
layer.CreateFeature(feat)
