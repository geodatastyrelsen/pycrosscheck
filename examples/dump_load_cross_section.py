import logging
import os

from pycrosscheck.common.logging.logging import LoggingSetup
from pycrosscheck.common.testing.testing import Testing
from pycrosscheck.cross_section import CrossSection

LoggingSetup.set_logging(ns_list=["pycrosscheck"])
logger = logging.getLogger(__name__)

testing = Testing(root_folder=os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
json_path = os.path.join(testing.output_data_folder(), "test_cross_section.json")
logger.debug("output json: %s" % json_path)

cs_orig = CrossSection()
cs_orig.station.name = "StationFile"
cs_orig.station.index = 1
cs_orig.object.name = "ObjectFile"
cs_orig.object.index = 999
cs_orig.cell_size = 99

logger.debug(cs_orig)

cs_orig.dump(path=json_path)

cs_clone = CrossSection()
cs_clone.load(path=json_path)

logger.debug("cs_orig == cs_clone: %s" % (cs_orig.as_dict() == cs_clone.as_dict()))
