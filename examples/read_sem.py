import logging

from pycrosscheck.common.logging.logging import LoggingSetup
from pycrosscheck.sem.sem import Sem

LoggingSetup.set_logging(ns_list=["pycrosscheck"])
logger = logging.getLogger(__name__)

# edit to a valid SEM path
sem_path = r"C:\_work\semantics\test\fau\test_1m.sem"
logger.debug('input: %s' % sem_path)

check_file_legend = True
show_bathy = True  # It can take a few minutes!

sem = Sem(path=sem_path)
sem.check_variant()
sem.read_header()
sem.read_file_legends()

if check_file_legend:
    sem.print_file_legends()
    sem.check_file_legends()

if show_bathy:
    sem.read_bathy()
    sem.show_bathy()
    sem.show_bathy_hist()
