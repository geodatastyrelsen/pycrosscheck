import logging
import math
from collections import defaultdict

logger = logging.getLogger(__name__)


class Roughness:

    @classmethod
    def calculate_roughness(cls, dtm_cols: int, dtm_rows: int, station_depths: defaultdict,
                            nr_of_depths: defaultdict) -> float:
        roughness = 0.0
        stat_roughness = 0.0
        nr_of_row_diffs = 0

        for r in range(dtm_rows):
            index = r * dtm_cols
            for c in range(1, dtm_cols):
                if (nr_of_depths[index + c - 1] > 0) and (nr_of_depths[index + c] > 0):
                    diff_row = station_depths[index + c - 1] - station_depths[index + c]
                    roughness += diff_row * diff_row
                    nr_of_row_diffs += 1
        logger.debug("nr of row diffs: %d" % nr_of_row_diffs)
        if nr_of_row_diffs > 0:
            stat_roughness = roughness / nr_of_row_diffs

        roughness = 0.0
        nr_of_col_diffs = 0
        for c in range(dtm_cols):
            for r in range(1, dtm_rows):
                if (nr_of_depths[c + (r - 1) * dtm_cols] > 0) and (nr_of_depths[c + r * dtm_cols] > 0):
                    diff_col = station_depths[c + (r - 1) * dtm_cols] - station_depths[c + r * dtm_cols]
                    roughness += diff_col * diff_col
                    nr_of_col_diffs += 1
        if nr_of_col_diffs > 0:
            stat_roughness += roughness / nr_of_col_diffs
        return math.sqrt(stat_roughness / 24.0)
