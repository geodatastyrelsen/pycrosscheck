import logging
from typing import Optional

from shapely.geometry import Polygon

from oshydro.sussie.survey.raw.raw import Raw

from pycrosscheck.geometry import Geometry

logger = logging.getLogger(__name__)


class FauFile:

    def __init__(self, fl: dict):
        self.fl = fl

        self.polygon = Geometry.make_polygon(fl=fl, plot=False)
        # logger.debug("polygon: %s" % self.polygon)

        self.raw = Raw(path=fl['fields']['path'])
        self.raw.check_variant()
        self.raw.read_header()
        self.nr_of_beams = self.raw.header.nr_of_beams
        self.nr_of_pings = self.raw.header.nr_of_pings
        # logger.debug("nr of pings/beams: %s/%s" % (self.nr_of_pings, self.nr_of_beams))
        self.max_non_linearity = self.raw.header.d['fields']['max_non_linearity']
        # logger.debug("max non linearity: %d" % self.max_non_linearity)
        self.transducer_depth = self.raw.header.d['fields']['transducer_depth']
        if (self.transducer_depth < 0) or (self.transducer_depth > 500):
            self.transducer_depth = 0
            logger.debug("Modified station's transducer depth: %d" % self.transducer_depth)
        self.raw.read_body()
        self.start_time = self.raw.body.get_sounding(ping=0, beam=0)['fields']['sec']
        # logger.debug("start time: %s" % self.start_time)

        self.min_dist = None  # type: Optional[float]
        self.max_dist = None  # type: Optional[float]

        self.cell_size = None  # type: Optional[float]

        self.search_start = None  # type: Optional[int]
        self.search_stop = None  # type: Optional[int]
        self.search_range = None  # type: Optional[int]

    def calculate_distances_from_intersection(self, intersection_polygon: Polygon) -> None:
        self.min_dist, self.max_dist = Geometry.calculate_min_max_dist(
            line_polygon=self.polygon, intersection_polygon=intersection_polygon)

    def calculate_cell_size(self) -> None:
        search_swath_start = round(
            ((self.max_dist + self.min_dist) / 4.0 / self.fl['fields']['bb_tilt_w']) * self.nr_of_pings
        ) - self.max_non_linearity
        # logger.debug("search_swath_start: %s" % self.search_swath_start)
        if search_swath_start > self.nr_of_pings:
            search_swath_start = round(
                (self.min_dist / 2.0 / self.fl['fields']['bb_tilt_w']) * self.nr_of_pings
            ) - self.max_non_linearity
            # logger.debug("new search_swath_start: %s" % self.search_swath_start)
        if search_swath_start < 0:
            search_swath_start = 0
            # logger.debug("new search_swath_start: %s" % self.search_swath_start)

        # fix the cell size at 1/20 of the avearge depth from the transducer
        avg_depth = 0.0
        nr_valid_depths = 0
        ping = search_swath_start
        self.cell_size = 100
        while (nr_valid_depths == 0) and (ping < self.nr_of_pings):
            for b in range(self.nr_of_beams):
                dg = self.raw.body.get_sounding(ping=ping, beam=b)
                if dg['fields']['quality'] < 128:
                    avg_depth += dg['fields']['depth']
                    nr_valid_depths += 1
            ping += 1
        if nr_valid_depths > 0:
            avg_depth /= nr_valid_depths
            self.cell_size = round((avg_depth - self.transducer_depth) * 0.05)
        # logger.info("avg depth: %.1f" % avg_depth)

        # cell size in centimeter
        if self.cell_size < 10:
            self.cell_size = 10

    def calculate_search_range(self):
        # search for start/stop ping in the f0 FAU
        self.search_start = round(
            self.min_dist / (2.0 * self.fl['fields']['bb_tilt_w']) * self.nr_of_pings
        ) - self.max_non_linearity
        if self.search_start < 0:
            self.search_start = 0
        self.search_stop = round(
            self.max_dist / (2.0 * self.fl['fields']['bb_tilt_w']) * self.nr_of_pings
        ) + self.max_non_linearity
        if self.search_stop > self.nr_of_pings:
            self.search_stop = self.nr_of_pings
        self.search_range = self.search_stop - self.search_start
        # logger.debug("f0 start/stop pings: %s, %s [%d]" % (self.search_start, self.search_stop, self.search_pings))
