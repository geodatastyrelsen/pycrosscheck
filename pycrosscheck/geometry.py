import logging
import math
from typing import Optional, Tuple

import matplotlib
from shapely.geometry import Point, Polygon, MultiPoint

matplotlib.use('TkAgg')

import matplotlib.pyplot as plt

logger = logging.getLogger(__name__)


class Geometry:

    @classmethod
    def make_polygon(cls, fl: dict, plot: bool = False) -> Polygon:
        ret_x = fl['fields']['bb_tilt_x'] / 100.0
        ret_y = fl['fields']['bb_tilt_y'] / 100.0
        # logger.debug("ret_x: %f, ret_y: %f" % (ret_x, ret_y))

        ret_len = fl['fields']['bb_tilt_w'] / 100.0
        ret_ang = fl['fields']['bb_tilt_ang']
        # logger.debug("ret_len: %f, ret_ang: %f (%f def)" % (ret_len, ret_ang, math.degrees(ret_ang)))
        ret_hgt = fl['fields']['bb_tilt_h'] / 100.0
        # logger.debug("ret_hgt: %f" % ret_hgt)
        ret_ang2 = ret_ang - math.radians(90.0)
        # logger.debug("ret_ang2: %f rad(%f deg)" % (ret_ang2, math.degrees(ret_ang2)))

        ret_dx = ret_len * math.cos(ret_ang)
        ret_dy = ret_len * math.sin(ret_ang)
        # logger.debug("ret_dx: %f, ret_dy: %f" % (ret_dx, ret_dy))
        ret_dx2 = ret_hgt * math.cos(ret_ang2)
        ret_dy2 = ret_hgt * math.sin(ret_ang2)
        # logger.debug("ret_dx2: %f, ret_dy2: %f" % (ret_dx2, ret_dy2))

        a_x = ret_x - ret_dx + ret_dx2
        a_y = ret_y - ret_dy + ret_dy2
        b_x = ret_x + ret_dx + ret_dx2
        b_y = ret_y + ret_dy + ret_dy2
        # f0_line = LineString([(a_x, a_y), (b_x, b_y)])

        c_x = ret_x - ret_dx - ret_dx2
        c_y = ret_y - ret_dy - ret_dy2
        d_x = ret_x + ret_dx - ret_dx2
        d_y = ret_y + ret_dy - ret_dy2
        # f1_line = LineString([(c_x, c_y), (d_x, d_y)])
        polygon = Polygon([(a_x, a_y), (b_x, b_y), (d_x, d_y), (c_x, c_y), (a_x, a_y)])

        if plot:
            # plt.plot(*f0_line.xy)
            # plt.plot(*f1_line.xy)
            plt.plot(*polygon.exterior.xy)
            plt.scatter(x=[ret_x, ], y=[ret_y, ])
            plt.ticklabel_format(style='plain')
            locs, labels = plt.xticks()
            plt.setp(labels, rotation=90)
            plt.axis('equal')
            plt.show()

        return polygon


    @classmethod
    def intersect_polygons(cls, p1: Polygon, p2: Polygon, plot: bool = False) -> Optional[Polygon]:

        f0f1 = p1.intersection(p2)
        if f0f1.is_empty:
            # logger.debug("no intersection")
            return None
        # logger.debug(f0f1)

        if plot:
            plt.plot(*p1.exterior.xy)
            plt.plot(*p2.exterior.xy)
            plt.scatter(*f0f1.exterior.xy)
            plt.ticklabel_format(style='plain')
            locs, labels = plt.xticks()
            plt.setp(labels, rotation=90)
            plt.axis('equal')
            plt.show()

        return f0f1

    @classmethod
    def are_points_collinear(cls, p0: Point, p1: Point, p2: Point) -> bool:
        x1, y1 = p1.x - p0.x, p1.y - p0.y
        x2, y2 = p2.x - p0.x, p2.y - p0.y
        # logger.debug("collinearity value: %s" % abs(x1 * y2 - x2 * y1))
        return abs(x1 * y2 - x2 * y1) < 0.001

    @classmethod
    def _get_min_max_dist_from_point(cls, start_pt: Point, intersection_polygon: Polygon) \
            -> Tuple[Optional[float], Optional[float]]:
        points = MultiPoint(intersection_polygon.boundary.coords)
        pre_point = None
        min_dist = None
        max_dist = None
        for point in points.geoms:
            # logger.debug(point)
            if pre_point is None:
                pre_point = point
                continue

            if Geometry.are_points_collinear(p0=start_pt, p1=pre_point, p2=point):
                # logger.debug("%s, %s, %s are collinear" % (start_pt, pre_point, point))
                min_dist = start_pt.distance(pre_point) * 100
                max_dist = start_pt.distance(point) * 100
                if min_dist > max_dist:
                    min_dist, max_dist = max_dist, min_dist
                break
            pre_point = point

        return min_dist, max_dist

    @classmethod
    def calculate_min_max_dist(cls, line_polygon: Polygon, intersection_polygon: Polygon) \
            -> Tuple[Optional[float], Optional[float]]:

        start_pt = Point(line_polygon.exterior.coords.xy[0][0], line_polygon.exterior.coords.xy[1][0])
        min_dist, max_dist = cls._get_min_max_dist_from_point(start_pt=start_pt,
                                                              intersection_polygon=intersection_polygon)

        if min_dist is None:
            start_pt = Point(line_polygon.exterior.coords.xy[0][3], line_polygon.exterior.coords.xy[1][3])
            min_dist, max_dist = cls._get_min_max_dist_from_point(start_pt=start_pt,
                                                                  intersection_polygon=intersection_polygon)

        return min_dist, max_dist
