import logging
import math
import sys
from collections import defaultdict

logger = logging.getLogger(__name__)


class LeastSquares:

    @classmethod
    def nll(cls, N: defaultdict, n: int, explim: int, sING: defaultdict):
        sum = 0.0
        I = (n * (n - 1) >> 1) - 1
        NI = 0.0
        tt = 0
        t = 1
        maxlim1 = -sys.maxsize - 1
        sing_size = 4

        for r in range(n):
            Ir = (r * (r + 1)) >> 1
            # logger.debug('r: %s -> Ir: %s' % (r, Ir))
            I = Ir - 1

            for c in range(r + 1):
                sum = 0.0
                Ic = (c * (c + 1)) >> 1
                I += 1
                for p in range(c):
                    sum -= N[Ir + p] * N[Ic + p]
                NI = N[I]
                # logger.debug('- c: %s -> Ic: %s, sum: %s, NI: %s' % (c, Ic, sum, NI))
                if r != c:
                    try:
                        N[I] = (NI + sum) / N[Ic + c]
                    except Exception as e:
                        logger.error('Issue at r,c: %s, %s' % (r, c))
                        # logger.error('Ic+c: %s' % (Ic + c))
                        # logger.error('N[Ic+c]: %s' % (N[Ic+c]))
                        raise e
                else:
                    if r != (n - 1):
                        tt += 1
                        if tt > sing_size:
                            t += 1
                            tt = 1
                        sING[t] <<= 1
                        expNI = math.frexp(NI)[1]
                        NI += sum
                        expNIred = math.frexp(NI)[1]
                        maxtest = expNI - expNIred
                        if maxtest > explim:
                            sing = 1
                        else:
                            sing = 0
                        if not sing and (NI > 0.0):
                            N[I] = math.sqrt(NI)
                        else:
                            N[I] = 1.7976931348623158e+308
                            sING[t] += 1

                        if maxtest > maxlim1:
                            maxlim1 = maxtest

        NI += sum
        pvv = NI
        maxlim = maxlim1 / 2048.0
        # logger.debug("maxlim: %s" % maxlim)

        c = n - 1
        while c > 0:
            I -= 1
            Ir = I
            Ic = ((c * (c + 1)) >> 1) - 1
            N[I] /= N[Ic]
            p = c - 1

            while p > 0:
                Ir -= 1
                Ic -= 1
                N[Ir] -= N[I] * N[Ic]

                p -= 1

            c -= 1

        return pvv
