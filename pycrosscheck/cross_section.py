import json
import logging
from collections import OrderedDict
from typing import Any, Dict, Optional

import shapely.wkt
from shapely.geometry import Polygon
from pycrosscheck.geometry import Geometry

logger = logging.getLogger(__name__)


class CrossSection:
    class TrackInfo:
        def __init__(self):
            self.name = str()
            self.roughness = 0.0  # seabed roughness in cross section
            self.status = 0  # 0 default, 1 crossline
            self.index = 0  # index in SEM
            self.start_time_track = 0  # in seconds
            self.avg_time_from_start = 0  # in seconds
            self.sv_error = 0.0  # in m/s
            self.center_error = 0.0  # in cm, vertical deviation caused by sv_error
            self.depth_error = 0.0  # estimated from moving average, in cm

    def __init__(self):
        self.station = CrossSection.TrackInfo()
        self.object = CrossSection.TrackInfo()
        self.center_northing = 0
        self.center_easting = 0
        self.cell_size = 0
        self.depth_difference = 0.0
        self.std_dev = 0.0  # sqrt(pvv/degfree)

        self.polygon = None  # type: Optional[Polygon]
        self.x_min = 0.0
        self.x_max = 0.0
        self.y_min = 0.0
        self.y_max = 0.0

    def intersect(self, p1: Polygon, p2: Polygon) -> None:
        self.polygon = Geometry.intersect_polygons(p1=p1, p2=p2, plot=False)
        if self.polygon is None:
            return

        self.x_min = min(self.polygon.exterior.coords.xy[0])
        self.x_max = max(self.polygon.exterior.coords.xy[0])
        self.y_min = min(self.polygon.exterior.coords.xy[1])
        self.y_max = max(self.polygon.exterior.coords.xy[1])
        # logger.info("X min: %.0f, max: %.0f" % (self.x_min, self.x_max))
        # logger.info("Y min: %.0f, max: %.0f" % (self.y_min, self.y_max))

    def bbox_polygon(self) -> Polygon:
        return Polygon([(self.x_min, self.y_min),
                        (self.x_min, self.y_max),
                        (self.x_max, self.y_max),
                        (self.x_max, self.y_min),
                        (self.x_min, self.y_min)])

    def as_dict(self) -> Dict[str, Any]:
        d = OrderedDict()

        for k in self.__dict__:
            if isinstance(self.__dict__[k], CrossSection.TrackInfo):
                d[k] = OrderedDict()
                for j in self.__dict__[k].__dict__:
                    d[k][j] = self.__dict__[k].__dict__[j]
            elif isinstance(self.__dict__[k], Polygon):
                d[k] = self.__dict__[k].wkt
            else:
                d[k] = self.__dict__[k]

        return d

    def dump(self, path: str) -> None:
        with open(path, 'w') as fod:
            json.dump(self.as_dict(), fod, indent=4, sort_keys=True)

    def load(self, path: str) -> None:
        with open(path) as fid:
            d = json.load(fid)
            for k in d.keys():
                if k in ["station", "object"]:
                    for j in d[k].keys():
                        self.__dict__[k].__dict__[j] = d[k][j]
                elif k in ["polygon"]:
                    if d[k] is None:
                        self.__dict__[k] = None
                    else:
                        self.__dict__[k] = shapely.wkt.loads(d[k])
                else:
                    self.__dict__[k] = d[k]

    def __str__(self):
        txt = "CrossSection\n"
        for k in self.__dict__:
            if isinstance(self.__dict__[k], CrossSection.TrackInfo):
                txt += "- %s\n" % k
                for j in self.__dict__[k].__dict__:
                    txt += "  - %s: %s\n" % (j, self.__dict__[k].__dict__[j])
            else:
                txt += "- %s: %s\n" % (k, self.__dict__[k])
        return txt
