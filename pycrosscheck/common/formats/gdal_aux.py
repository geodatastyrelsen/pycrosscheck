import logging
import os
import sys
from typing import Optional

from osgeo import gdal, osr

logger = logging.getLogger(__name__)


class GdalAux:

    version_shown = False
    error_handling = False
    exceptions = False
    gdal_data_fixed = False
    proj4_data_fixed = False

    def __init__(self, error_handling: bool = True, exceptions: bool = True,
                 fix_gdal_data: bool = True, fix_proj4_data: bool = True) -> None:
        if not self.version_shown:
            logger.debug("GDAL version: %s" % gdal.VersionInfo('VERSION_NUM'))
            self.version_shown = True

        if error_handling:
            self.set_error_handling()

        if exceptions:
            self.set_exceptions()

        if fix_gdal_data:
            self.check_gdal_data()

        if fix_proj4_data:
            self.check_proj4_data()

    @classmethod
    def set_error_handling(cls) -> None:
        if cls.error_handling:
            return

        # example GDAL error handler function
        def gdal_error_handler(err_class, err_num, err_msg):
            err_type = {
                gdal.CE_None: 'None',
                gdal.CE_Debug: 'Debug',
                gdal.CE_Warning: 'Warning',
                gdal.CE_Failure: 'Failure',
                gdal.CE_Fatal: 'Fatal'
            }
            err_msg = err_msg.replace('\n', ' ')
            err_class = err_type.get(err_class, 'None')
            raise RuntimeError('GDAL %s: %s -> %s' % (err_num, err_class, err_msg))

        gdal.PushErrorHandler(gdal_error_handler)
        cls.error_handling = True
        logger.debug("GDAL error handling: %s" % cls.error_handling)

    @classmethod
    def set_exceptions(cls) -> None:
        if cls.exceptions:
            return

        gdal.UseExceptions()
        cls.exceptions = True
        logger.debug("GDAL exceptions: %s" % cls.exceptions)

    @classmethod
    def check_gdal_data(cls) -> None:
        """ Check the correctness of os env GDAL_DATA """

        if cls.gdal_data_fixed:
            return

        if 'GDAL_DATA' in os.environ:

            # logger.debug("unset original GDAL_DATA = %s" % os.environ['GDAL_DATA'])
            del os.environ['GDAL_DATA']

        if 'GDAL_DRIVER_PATH' in os.environ:

            # logger.debug("unset original GDAL_DRIVER_PATH = %s" % os.environ['GDAL_DRIVER_PATH'])
            del os.environ['GDAL_DRIVER_PATH']

        gdal_data_path0 = os.path.join(os.path.dirname(gdal.__file__), 'osgeo', 'data', 'gdal')
        s57_agencies_csv_path0 = os.path.join(gdal_data_path0, 's57agencies.csv')
        if os.path.exists(s57_agencies_csv_path0):

            gdal.SetConfigOption('GDAL_DATA', gdal_data_path0)
            logger.debug("GDAL_DATA = %s" % gdal.GetConfigOption('GDAL_DATA'))
            cls.gdal_data_fixed = True
            return

        gdal_data_path1 = os.path.join(os.path.dirname(gdal.__file__), 'data', 'gdal')
        s57_agencies_csv_path1 = os.path.join(gdal_data_path1, 's57agencies.csv')
        if os.path.exists(s57_agencies_csv_path1):

            gdal.SetConfigOption('GDAL_DATA', gdal_data_path1)
            logger.debug("GDAL_DATA = %s" % gdal.GetConfigOption('GDAL_DATA'))
            cls.gdal_data_fixed = True
            return

        # anaconda specific (Win)
        gdal_data_path2 = os.path.join(cls._python_path(), 'Library', 'data')
        s57_agencies_csv_path2 = os.path.join(gdal_data_path2, 's57agencies.csv')
        if os.path.exists(s57_agencies_csv_path2):

            gdal.SetConfigOption('GDAL_DATA', gdal_data_path2)
            logger.debug("GDAL_DATA = %s" % gdal.GetConfigOption('GDAL_DATA'))
            cls.gdal_data_fixed = True
            return

        # anaconda specific (Win)
        gdal_data_path3 = os.path.join(cls._python_path(), 'Library', 'share', 'gdal')
        s57_agencies_csv_path3 = os.path.join(gdal_data_path3, 's57agencies.csv')
        if os.path.exists(s57_agencies_csv_path3):

            gdal.SetConfigOption('GDAL_DATA', gdal_data_path3)
            logger.debug("GDAL_DATA = %s" % gdal.GetConfigOption('GDAL_DATA'))
            cls.gdal_data_fixed = True
            return

        # anaconda specific (Linux)
        gdal_data_path4 = os.path.join(cls._python_path(), 'share', 'gdal')
        s57_agencies_csv_path4 = os.path.join(gdal_data_path4, 's57agencies.csv')
        if os.path.exists(s57_agencies_csv_path4):

            gdal.SetConfigOption('GDAL_DATA', gdal_data_path4)
            logger.debug("GDAL_DATA = %s" % gdal.GetConfigOption('GDAL_DATA'))
            cls.gdal_data_fixed = True
            return

        # TODO: add more cases to find GDAL_DATA

        raise RuntimeError("Unable to locate GDAL data at:\n- %s\n- %s\n- %s\n- %s\n- %s"
                           % (gdal_data_path0, gdal_data_path1, gdal_data_path2, gdal_data_path3, gdal_data_path4))

    @classmethod
    def check_proj4_data(cls) -> None:
        """ Check the correctness of os env PROJ_LIB """

        if cls.proj4_data_fixed:
            return

        if 'PROJ_LIB' in os.environ:

            # logger.debug("unset original PROJ_LIB = %s" % os.environ['PROJ_LIB'])
            del os.environ['PROJ_LIB']

        try:
            import pyproj

            if hasattr(pyproj, 'pyproj_datadir'):
                # noinspection PyTypeChecker
                proj_path = os.path.join(pyproj.pyproj_datadir, "epsg")
                if os.path.exists(proj_path):
                    logger.debug("PROJ_LIB = %s" % proj_path)
                    return

            proj4_data_path1 = os.path.join(os.path.dirname(pyproj.__file__), 'data')
            epsg_path1 = os.path.join(proj4_data_path1, 'epsg')
            if os.path.exists(epsg_path1):

                os.environ['PROJ_LIB'] = proj4_data_path1
                if hasattr(pyproj, 'pyproj_datadir'):
                    pyproj.pyproj_datadir = proj4_data_path1
                logger.debug("PROJ_LIB = %s" % os.environ['PROJ_LIB'])
                cls.proj4_data_fixed = True
                return

        except ImportError:
            proj4_data_path1 = "[SKIP]"

        # anaconda specific (Win)
        proj4_data_path2 = os.path.join(cls._python_path(), 'Library', 'data')
        epsg_path2 = os.path.join(proj4_data_path2, 'epsg')
        if os.path.exists(epsg_path2):

            os.environ['PROJ_LIB'] = proj4_data_path2
            try:
                if hasattr(pyproj, 'pyproj_datadir'):
                    pyproj.pyproj_datadir = proj4_data_path2
            except Exception:
                pass
            logger.debug("PROJ_LIB = %s" % os.environ['PROJ_LIB'])
            cls.proj4_data_fixed = True
            return

        # anaconda specific (Win)
        proj4_data_path3 = os.path.join(cls._python_path(), 'Library', 'share')
        epsg_path3 = os.path.join(proj4_data_path3, 'epsg')
        if os.path.exists(epsg_path3):

            os.environ['PROJ_LIB'] = proj4_data_path3
            try:
                if hasattr(pyproj, 'pyproj_datadir'):
                    pyproj.pyproj_datadir = proj4_data_path3
            except Exception:
                pass
            logger.debug("PROJ_LIB = %s" % os.environ['PROJ_LIB'])
            cls.proj4_data_fixed = True
            return

        # anaconda specific (Linux)
        proj4_data_path4 = os.path.join(cls._python_path(), 'share')
        epsg_path4 = os.path.join(proj4_data_path4, 'epsg')
        if os.path.exists(epsg_path4):

            os.environ['PROJ_LIB'] = proj4_data_path4
            try:
                if hasattr(pyproj, 'pyproj_datadir'):
                    pyproj.pyproj_datadir = proj4_data_path4
            except Exception:
                pass
            logger.debug("PROJ_LIB = %s" % os.environ['PROJ_LIB'])
            cls.proj4_data_fixed = True
            return

        # anaconda specific (Linux)
        proj4_data_path5 = os.path.join(cls._python_path(), 'share', 'proj')
        proj_db_path5 = os.path.join(proj4_data_path5, 'proj.db')
        if os.path.exists(proj_db_path5):

            os.environ['PROJ_LIB'] = proj4_data_path5
            try:
                if hasattr(pyproj, 'pyproj_datadir'):
                    pyproj.pyproj_datadir = proj4_data_path5
            except Exception:
                pass
            logger.debug("PROJ_LIB = %s" % os.environ['PROJ_LIB'])
            cls.proj4_data_fixed = True
            return

        # anaconda specific (Win)
        proj4_data_path6 = os.path.join(cls._python_path(), 'Library', 'share', 'proj')
        proj_db_path6 = os.path.join(proj4_data_path6, 'proj.db')
        if os.path.exists(proj_db_path6):

            os.environ['PROJ_LIB'] = proj4_data_path6
            try:
                if hasattr(pyproj, 'pyproj_datadir'):
                    pyproj.pyproj_datadir = proj4_data_path6
            except Exception:
                pass
            logger.debug("PROJ_LIB = %s" % os.environ['PROJ_LIB'])
            cls.proj4_data_fixed = True
            return

        try:
            # noinspection PyUnresolvedReferences
            import conda

            conda_file_dir = conda.__file__
            conda_dir = conda_file_dir.split('lib')[0]
            proj4_data_path999 = os.path.join(os.path.join(conda_dir, 'share'), 'proj')
            epsg_path999 = os.path.join(proj4_data_path999, 'epsg')
            if os.path.exists(epsg_path999):

                os.environ['PROJ_LIB'] = proj4_data_path999
                try:
                    if hasattr(pyproj, 'pyproj_datadir'):
                        pyproj.pyproj_datadir = proj4_data_path999
                except Exception:
                    pass
                logger.debug("PROJ_LIB = %s" % os.environ['PROJ_LIB'])
                cls.proj4_data_fixed = True
                return

        except Exception as e:
            logger.warning("%s" % e)

        # TODO: add more cases to find PROJ_LIB

        raise RuntimeError("Unable to locate PROJ4 data at:\n- %s\n- %s\n- %s\n- %s\n- %s\n- %s\n- Conda/share/proj"
                           % (proj4_data_path1, proj4_data_path2, proj4_data_path3, proj4_data_path4, proj4_data_path5,
                              proj4_data_path6))

    @classmethod
    def epsg_from_wkt(cls, wkt: str) -> Optional[int]:
        srs = osr.SpatialReference()
        srs.ImportFromWkt(wkt)
        if srs.IsCompound():
            logger.debug("Compound CRS")
            start_token = "PROJCS["
            end_token = "VERT_CS["
            wkt = wkt[wkt.find(start_token):wkt.find(end_token)]
            wkt = wkt.strip()[:-1]  # the ending comma
            # logger.debug(wkt)
            srs.ImportFromWkt(wkt)
        srs.AutoIdentifyEPSG()
        try:
            return int(srs.GetAuthorityCode(None))
        except Exception as e:
            logger.warning("Unable to retrieve EPSG code: %s" % e)
            return None

    @classmethod
    def _is_windows(cls) -> bool:
        return (sys.platform == 'win32') or (os.name == "nt")

    @classmethod
    def _python_path(cls) -> str:
        """ Return the python site-specific directory prefix (the temporary folder for PyInstaller) """

        # required by PyInstaller
        if hasattr(sys, '_MEIPASS'):
            if cls._is_windows():
                import win32api
                # noinspection PyProtectedMember
                return win32api.GetLongPathName(sys._MEIPASS)
            else:
                # noinspection PyProtectedMember
                return sys._MEIPASS

        # check if in a virtual environment
        if hasattr(sys, 'real_prefix'):

            if cls._is_windows():
                import win32api
                # noinspection PyProtectedMember
                return win32api.GetLongPathName(sys.real_prefix)
            else:
                return sys.real_prefix

        return sys.prefix
