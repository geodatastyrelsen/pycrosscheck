import logging
import os
import subprocess
from typing import Optional

from appdirs import user_data_dir

logger = logging.getLogger(__name__)


class Common:

    def __init__(self, tool_name: str):
        self._tool_name = tool_name
        self._tool_folder = None

    @classmethod
    def explore_folder(cls, path: str) -> bool:
        """Open the passed path using OS-native commands"""

        if not os.path.exists(path):
            logger.warning('invalid path to folder: %s' % path)
            return False

        path = os.path.normpath(path)

        subprocess.call(['explorer', path])
        return True

    def set_tool_folder(self, path: str) -> None:
        if not os.path.exists(path):
            raise RuntimeError('Unable to locate %s' % path)
        self._tool_folder = path

    @property
    def tool_folder(self) -> str:
        if self._tool_folder is None:
            self._tool_folder = user_data_dir(appname=self._tool_name, appauthor="HydroTeam")
            if not os.path.exists(self._tool_folder):  # create it if it does not exist
                os.makedirs(self._tool_folder)
        return self._tool_folder

    @property
    def inputs_folder(self) -> str:
        folder = os.path.join(self.tool_folder, "inputs")
        if not os.path.exists(folder):  # create it if it does not exist
            os.makedirs(folder)
        return folder

    def open_inputs_folder(self) -> None:
        self.explore_folder(self.inputs_folder)

    @property
    def outputs_folder(self) -> str:
        folder = os.path.join(self.tool_folder, "outputs")
        if not os.path.exists(folder):  # create it if it does not exist
            os.makedirs(folder)
        return folder

    def open_outputs_folder(self) -> None:
        self.explore_folder(self.outputs_folder)

    @property
    def backup_folder(self) -> str:
        folder = os.path.join(self.tool_folder, "backup")
        if not os.path.exists(folder):  # create it if it does not exist
            os.makedirs(folder)
        return folder

    def open_backup_folder(self) -> None:
        self.explore_folder(self.backup_folder)

    @classmethod
    def files(cls, folder: str, ext: str, subfolder: Optional[str] = None, start_with: Optional[str] = None,
              walk: bool = True) -> list:
        file_list = list()
        for root, _, files in os.walk(folder):

            # logger.info("root: %s, folder: %s" % (root, subfolder))
            if subfolder is not None:
                if subfolder not in root:
                    continue

            for f in files:
                if f.endswith(ext):

                    if start_with is None:
                        file_list.append(os.path.join(root, f))
                    else:
                        if start_with == f[:len(start_with)]:
                            file_list.append(os.path.join(root, f))

            if walk is False:
                break

        return file_list
