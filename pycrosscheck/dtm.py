import logging
import math
from collections import defaultdict
from typing import Optional, List

from pycrosscheck.fau_file import FauFile
from pycrosscheck.cross_section import CrossSection
from pycrosscheck.roughness import Roughness

logger = logging.getLogger(__name__)


class DTM:

    def __init__(self, f: FauFile, cs: CrossSection):
        self._f = f
        self._cs = cs

        # determine the size of the intersection DTM
        self.dtm_cols = int((self._cs.x_max - self._cs.x_min) * 100 / self._f.cell_size)
        self.dtm_rows = int((self._cs.y_max - self._cs.y_min) * 100 / self._f.cell_size)

        self.sum_of_depths = defaultdict(float)
        self.sum_of_curv = defaultdict(float)
        self.nr_of_depths = defaultdict(int)
        self.sec_from_start = defaultdict(int)
        self.station_depths = defaultdict(float)
        self.station_curvs = defaultdict(float)
        self.roughness = None  # type: Optional[float]

    def populate(self, filter: Optional[List[str]] = None):
        ping = self._f.search_start
        self.sum_of_depths = defaultdict(float)
        self.sum_of_curv = defaultdict(float)
        self.nr_of_depths = defaultdict(int)
        self.sec_from_start = defaultdict(int)
        while ping < self._f.search_stop:
            # logger.debug("ping %d" % ping)
            for b in range(self._f.nr_of_beams):
                dg = self._f.raw.body.get_sounding(ping=ping, beam=b)
                if dg['fields']['quality'] >= 128:
                    continue

                row = int((dg['fields']['n'] - self._cs.y_min * 100) / self._f.cell_size)
                if (row < 0) or (row >= self.dtm_rows):
                    continue

                col = int((dg['fields']['e'] - self._cs.x_min * 100) / self._f.cell_size)
                if (col < 0) or (col >= self.dtm_cols):
                    continue

                index_dtm = col + row * self.dtm_cols
                if filter is not None:
                    if index_dtm not in filter:
                        continue

                d_below_trans = dg['fields']['depth'] - self._f.transducer_depth
                # logger.debug("index dtm: %d" % index_dtm)
                tan_angle = math.tan(math.radians(dg['fields']['angle'] / 100.0))
                self.sum_of_depths[index_dtm] += dg['fields']['depth']
                self.sum_of_curv[index_dtm] += float(d_below_trans * (1.0 - tan_angle * tan_angle))
                self.nr_of_depths[index_dtm] += 1
                self.sec_from_start[index_dtm] += dg['fields']['sec'] - self._f.start_time

            ping += 1

        self.station_depths = defaultdict(float)
        self.station_curvs = defaultdict(float)
        self.station_sec_from_start = defaultdict(float)
        logger.debug("populated %d DTM nodes" % len(self.nr_of_depths))
        for key in self.nr_of_depths.keys():
            self.station_depths[key] = self.sum_of_depths[key] / self.nr_of_depths[key]
            self.station_curvs[key] = -self.sum_of_curv[key] / self.nr_of_depths[key]
            # logger.debug("station depth: %f, curv: %f" % (self.station_depths[key], self.station_curvs[key]))
            self.station_sec_from_start[key] = self.sec_from_start[key] / self.nr_of_depths[key]

        self.roughness = Roughness.calculate_roughness(dtm_cols=self.dtm_cols, dtm_rows=self.dtm_rows,
                                                       station_depths=self.station_depths,
                                                       nr_of_depths=self.nr_of_depths)
