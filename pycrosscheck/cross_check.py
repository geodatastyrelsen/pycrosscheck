import logging
import math
import os
import shutil
from collections import defaultdict
from typing import Optional

import matplotlib
import numpy as np
from osgeo import ogr

matplotlib.use('TkAgg')

from datetime import datetime

from pycrosscheck.sem.sem import Sem
from pycrosscheck.cross_section import CrossSection
from pycrosscheck.fau_file import FauFile
from pycrosscheck.dtm import DTM
from pycrosscheck.least_squares import LeastSquares

from pycrosscheck.common.common import Common
from pycrosscheck.common.formats.gdal_aux import GdalAux

logger = logging.getLogger(__name__)


class CrossCheck:

    def __init__(self, path: str, only_extract: bool = False):
        if not os.path.exists(path):
            raise RuntimeError("Unable to locate SEM file: %s" % path)
        self._p = path
        self._c = Common(tool_name="CrossCheck")
        self._output_folder = os.path.join(self._c.outputs_folder, os.path.splitext(os.path.basename(self._p))[0])
        self._only_extract = only_extract
        if self._only_extract:
            if not os.path.exists(self._output_folder):
                raise RuntimeError("Unable to locate output folder: %s" % self._output_folder)
        else:
            if os.path.exists(self._output_folder):
                shutil.rmtree(self._output_folder)
            os.mkdir(self._output_folder)
        logger.debug("output folder: %s" % self._output_folder)

        self._sem = Sem(path=path)
        self._sem.check_variant()
        self._sem.read_header()
        # self._sem.print_header()
        self._sem.check_header_size()
        self._sem.read_file_legends()

        self._dev_angle = 10.0
        self._cos_limit_angle = math.cos(math.radians(90.0 - self._dev_angle))
        self._offsets_only = False

        self._save_intersections = False

        self._s = None  # type: Optional[FauFile]  # The current Station File
        self._o = None  # type: Optional[FauFile]  # The current Object file
        self._cs = None  # type: Optional[CrossSection]  # The current cross section
        self._sd = None  # type: Optional[DTM]  # The station DTM
        self._od = None  # type: Optional[DTM]  # The object DTM

    @property
    def sem(self) -> Sem:
        return self._sem

    def open_output_folder(self) -> None:
        self._c.explore_folder(self._output_folder)

    def run(self, dev_angle: float = 10.0, offsets_only: bool = False, save_intersections: bool = False) -> None:

        if not self._only_extract:

            self._save_intersections = save_intersections

            logger.info("Cross-checking %d FAU files ..." % (len(self.sem.file_legends), ))
            start_time = datetime.now()

            self._dev_angle = dev_angle
            self._cos_limit_angle = math.cos(math.radians(90.0 - self._dev_angle))
            logger.debug("maximum deviation from orthogonality: %.1f -> test limit: %.6f"
                         % (self._dev_angle, self._cos_limit_angle))

            self._offsets_only = offsets_only
            logger.debug("offset-only mode: %s" % self._offsets_only)

            for idx, fl in enumerate(self.sem.file_legends):
                self._cross_check_station(fl=fl, idx=idx)

            computation_time = (datetime.now() - start_time).total_seconds() / 60.0
            logger.info("Cross-checking %d FAU files ... DONE! -> time: %.2f mins"
                        % (len(self.sem.file_legends), computation_time))

        self.extract_info()

    def _cross_check_station(self, fl: dict, idx: int) -> None:

        logger.debug("#%d station file: %s" % (idx, fl['fields']['path']))
        if not os.path.exists(fl['fields']['path']):
            raise RuntimeError("Unable to locate FAU file: %s" % fl['fields']['path'])

        if fl['fields']['stat_is_valid'] != 2:
            logger.info("The station's rotated bbox is invalid -> SKIP")

        self._s = FauFile(fl=fl)

        for jdx, fl2 in enumerate(self.sem.file_legends):
            if not os.path.exists(fl2['fields']['path']):
                raise RuntimeError("Unable to locate FAU file: %s" % fl2['fields']['path'])
            self._cross_check_object(fl=fl, idx=idx, fl2=fl2, jdx=jdx)

    def _cross_check_object(self, fl: dict, idx: int, fl2: dict, jdx: int) -> None:
        if jdx <= idx:
            return

        if fl2['fields']['stat_is_valid'] != 2:
            return

        if math.fabs(math.cos(fl2['fields']['bb_tilt_ang'] - fl['fields']['bb_tilt_ang'])) > self._cos_limit_angle:
            return

        self._o = FauFile(fl=fl2)

        self._cs = CrossSection()
        self._cs.intersect(p1=self._s.polygon, p2=self._o.polygon)
        if self._cs.polygon is None:
            return

        logger.debug("- #%d object file: %s" % (jdx, fl2['fields']['path']))
        logger.debug("intersection: %s" % self._cs.polygon)

        self._s.calculate_distances_from_intersection(intersection_polygon=self._cs.polygon)
        if self._s.min_dist is None:
            logger.warning("Unable to calculate the minimum distance for station")
            return
        # logger.debug("stations distances -> min: %.1f, max: %.1f" % (self._s.min_dist, self._s.max_dist))

        self._s.calculate_cell_size()
        logger.debug("cell size: %d" % self._s.cell_size)

        self._s.calculate_search_range()

        self._sd = DTM(f=self._s, cs=self._cs)
        logger.debug("station dtm size: (%d, %d)" % (self._sd.dtm_rows, self._sd.dtm_cols))
        self._sd.populate()
        logger.debug("station roughness: %.6f" % self._sd.roughness)

        # open the collided FAU
        self._o = FauFile(fl=fl2)

        self._o.calculate_distances_from_intersection(intersection_polygon=self._cs.polygon)
        if self._o.min_dist is None:
            logger.warning("Unable to calculate the minimum distance for object")
            return
        # logger.debug("object distances -> min: %.1f, max: %.1f" % (self._o.min_dist, self._o.max_dist))

        # use the same cell size as the station
        self._o.cell_size = self._s.cell_size

        self._o.calculate_search_range()

        self._od = DTM(f=self._o, cs=self._cs)
        self._od.populate(filter=list(self._sd.station_depths.keys()))
        logger.debug("object roughness: %.6f" % self._od.roughness)

        self._finalize()

        if self._save_intersections:
            self._save_current_intersection()

    def _finalize(self) -> None:
        ok = True

        center_row = 0
        center_col = 0
        nr_obs = 0
        dtm_distances = defaultdict(float)
        avg_below_transducer = 0.0
        station_avg_time_start = 0.0
        object_avg_time_start = 0.0
        sum_distance = 0.0
        ss = 0.0
        n = defaultdict(float)
        for key in self._od.nr_of_depths.keys():
            if self._od.station_depths[key] == 0.0:
                continue
            center_row += int(key // self._od.dtm_cols)
            center_col += int(key % self._od.dtm_cols)
            obj_curvature = self._od.sum_of_curv[key] / self._od.nr_of_depths[key]
            dtm_distance = self._sd.station_depths[key] - self._od.station_depths[key]
            dtm_distances[nr_obs] = dtm_distance
            avg_below_transducer += self._sd.station_depths[key]
            nr_obs += 1
            station_avg_time_start += self._sd.station_sec_from_start[key]
            object_avg_time_start += self._od.station_sec_from_start[key]
            sum_distance += dtm_distance
            ss = dtm_distance * dtm_distance
            n[0] += self._sd.station_curvs[key] * self._sd.station_curvs[key]
            n[1] += self._sd.station_curvs[key] * obj_curvature
            n[2] += obj_curvature * obj_curvature
            n[3] += self._sd.station_curvs[key]
            n[4] += obj_curvature
            n[5] += 1.0
            n[6] += self._sd.station_curvs[key] * dtm_distance
            n[7] += obj_curvature * dtm_distance
            n[8] += dtm_distance
            n[9] += dtm_distance * dtm_distance

        if nr_obs == 0:
            return

        sub_determinant = n[0] * n[2] - n[1] * n[1]
        determinant = n[0] * n[2] * n[5] + 2 * n[1] * n[4] * n[3] \
                      - (n[2] * n[3] * n[3] + n[1] * n[1] * n[5] + n[4] * n[4] * n[0])

        # determine the center of crossing
        center_row /= nr_obs
        center_col /= nr_obs
        center_y = center_row * self._o.cell_size + self._cs.y_min
        center_x = center_col * self._o.cell_size + self._cs.x_min
        avg_below_transducer /= nr_obs
        avg_below_transducer -= self._s.transducer_depth

        # set the lower limit of valid cells for a crossing
        swath_scale = 3.4  # 2*tan(60)
        min_nr_of_adjusted_cells = int(0.7 * swath_scale * avg_below_transducer / self._o.cell_size)
        min_nr_of_adjusted_cells *= min_nr_of_adjusted_cells
        if self._offsets_only:
            min_nr_of_adjusted_cells = 200
        # logger.debug("minimum number of adjusted cells: %d" % min_nr_of_adjusted_cells)
        station_idx = self._s.fl['fields']['index']
        object_idx = self._o.fl['fields']['index']
        logger.debug("station idx: %d, object idx: %d" % (station_idx, object_idx))
        mean_distance = sum_distance / nr_obs
        # logger.debug("mean distance: %f" % mean_distance)
        variance_distance = (ss - nr_obs * mean_distance * mean_distance) / (nr_obs - 1)

        self._cs.station.roughness = self._sd.roughness
        self._cs.object.roughness = self._od.roughness
        pvv = 0.0
        variance_depth_difference = 0.0

        self._s.fl['fields']['is_in_sem'] = True
        self._cs.station.name = os.path.splitext(os.path.basename(self._s.fl['fields']['path']))[0]
        self._cs.station.index = station_idx + 1
        self._cs.station.depth_error = -999.99  # dummy value

        if nr_obs > min_nr_of_adjusted_cells:
            singularity = defaultdict(int)
            for i in range(10):
                singularity[i] = 0
            maxlim = 0.0
            for t1 in range(10):
                singularity[t1] = 0
            pvv = LeastSquares.nll(N=n, n=4, explim=15, sING=singularity)
            pvv /= (nr_obs - 3)
            # logger.debug("pvv: %s" % pvv)
            for i in range(10):
                if singularity[i] != 0:
                    ok = False
        station_avg_time_start /= nr_obs
        object_avg_time_start /= nr_obs
        if determinant > 0.0:
            variance_depth_difference = pvv * sub_determinant / determinant
        std_dev_depth_difference = math.sqrt(variance_depth_difference)
        # logger.debug("std dev of depth difference: %f" % std_dev_depth_difference)

        # logger.debug("dtm distances: %s" % (dtm_distances,))
        dtm_distances_arr = np.array(list(dtm_distances.values()))
        # plt.hist(dtm_distances_arr, density=False, bins=30, histtype='step')  # density=True for probability
        # plt.ylabel('Count')
        # plt.xlabel('Depth')
        # plt.show()
        median_dtm_distance = np.median(dtm_distances_arr)
        logger.debug("median dtm distance: %s" % median_dtm_distance)
        abs_dist_dev = np.fabs(dtm_distances_arr - median_dtm_distance)
        fractile_95 = np.percentile(abs_dist_dev, 95)
        logger.debug("percentile 95: %s" % fractile_95)

        if ok:
            self._cs.object.name = os.path.splitext(os.path.basename(self._o.fl['fields']['path']))[0]
            self._cs.object.index = object_idx + 1
            self._cs.center_northing = center_y
            self._cs.center_easting = center_x
            self._cs.station.start_time_track = self._s.start_time
            self._cs.station.avg_time_from_start = int(station_avg_time_start)
            self._cs.object.start_time_track = self._o.start_time
            self._cs.object.avg_time_from_start = int(object_avg_time_start)
            self._cs.cell_size = self._o.cell_size
            if self._offsets_only:
                self._cs.station.center_error = 0.0
                self._cs.station.depth_error = -999.9
                self._cs.station.sv_error = 0.0
                self._cs.object.center_error = 0.0
                self._cs.object.depth_error = -999.9
                self._cs.object.sv_error = 0.0
                self._cs.depth_difference = median_dtm_distance
                self._cs.std_dev = fractile_95 / 2.0
            else:
                self._cs.station.center_error = n[6] * avg_below_transducer
                self._cs.station.depth_error = -999.9
                self._cs.station.sv_error = n[6] * 1465.0
                self._cs.object.center_error = n[7] * avg_below_transducer
                self._cs.object.depth_error = -999.9
                self._cs.object.sv_error = n[7] * 1465.0
                self._cs.depth_difference = n[8]
                self._cs.std_dev = math.sqrt(pvv)

        # logger.debug(section_info)
        section_info_path = os.path.join(self._output_folder,
                                         "%03dx%03d.json" % (self._cs.station.index, self._cs.object.index))
        self._cs.dump(path=section_info_path)
        logger.debug("section info dump: %s" % section_info_path)

    def _save_current_intersection(self):
        GdalAux()
        driver = ogr.GetDriverByName('GPKG')
        section_info_gpkg = os.path.join(
            self._output_folder, "%03dx%03d.gpkg" % (self._cs.station.index, self._cs.object.index))
        ds = driver.CreateDataSource(section_info_gpkg)
        layer = ds.CreateLayer('%03dx%03d' % (self._cs.station.index, self._cs.object.index), None,
                               ogr.wkbPolygon)
        defn = layer.GetLayerDefn()

        feat = ogr.Feature(defn)
        geom = ogr.CreateGeometryFromWkb(self._s.polygon.wkb)
        feat.SetGeometry(geom)
        layer.CreateFeature(feat)

        feat = ogr.Feature(defn)
        geom = ogr.CreateGeometryFromWkb(self._o.polygon.wkb)
        feat.SetGeometry(geom)
        layer.CreateFeature(feat)

        feat = ogr.Feature(defn)
        geom = ogr.CreateGeometryFromWkb(self._cs.bbox_polygon().wkb)
        feat.SetGeometry(geom)
        layer.CreateFeature(feat)

        feat = geom = None  # destroy these
        ds = layer = feat = geom = None

    def extract_info(self):
        json_files = self._c.files(folder=self._output_folder, ext=".json")
        logger.debug("retrieved %d cross-section files" % len(json_files))

        output_file = os.path.join(self._output_folder, "CrossLineCheck.txt")
        with open(file=output_file, mode="wt") as fod:
            fod.write("*****************************************************************************\n")
            fod.write("cell time from   S/V     below    vert.     Stat      Obj      std   SEMI  name\n")
            fod.write("size  start     error   transd.   displ.    Rough     Rough    dev    nr.  track\n")

            cs = CrossSection()
            pre_start_file = None
            for json_file in json_files:
                start_file = os.path.basename(json_file).split("x")[0]
                cs.load(path=json_file)

                if start_file != pre_start_file:
                    logger.debug("new station: %s" % start_file)
                    fod.write("\n %4d a   relative S/V %s\n" % (cs.station.index, cs.station.name))

                second = cs.station.avg_time_from_start
                hour = second // 3600
                second -= hour * 3600
                minute = second // 60
                second -= minute * 60

                fod.write("%4d %02d:%02d:%02d %5.1fm/s % 6.1fcm % 6.1fcm  %5.1fcm  %5.1fcm   %5.1fcm %4d %s\n"
                          % (cs.cell_size, hour, minute, second, cs.station.sv_error, cs.station.center_error,
                             cs.depth_difference, cs.station.roughness, cs.object.roughness, cs.std_dev,
                             cs.object.index, cs.object.name))

                pre_start_file = start_file
