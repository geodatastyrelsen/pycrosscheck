from __future__ import annotations

import logging
import os
import struct
from datetime import datetime
from typing import TYPE_CHECKING, Optional

if TYPE_CHECKING:
    from pycrosscheck.sem.sem_header import SemHeader

logger = logging.getLogger(__name__)


class SemFileLegend:

    def __init__(self, path: str, variant: dict, header: Optional[SemHeader]):
        self._write_only = False
        if not os.path.exists(path):
            logger.debug('write-only mode')
            self._write_only = True

        self._p = path
        self._v = variant
        self._h = header
        self._l = list()

        self._keys = list(self._v['file_legend']['fields'].keys())
        self._types = '<'
        for k in self._keys:
            # print(k)
            self._types += self._v['file_legend']['fields'][k]['type']
        self._dg_size = struct.calcsize(self._types)
        # logger.debug('file legend types: %s -> %d' % (self._types, self._dg_size))

        self._r = open(self._p, mode='rb')
        # logger.debug('opened in read mode: %s' % os.path.basename(self._p))

    @classmethod
    def print(cls, fl: dict):
        for k, v in fl['fields'].items():
            if isinstance(v, bytes):
                logger.debug(" - %s -> %s" % (k, v.decode(errors='backslashreplace')))
            else:
                if k == 'sec':
                    logger.debug(" - %s -> %s (%s)" % (k, v, datetime.utcfromtimestamp(v)))
                else:
                    logger.debug(" - %s -> %s" % (k, v))

    @classmethod
    def size(cls, variant: dict) -> int:
        keys = variant['file_legend']['fields'].keys()
        types = str()
        types += '<'

        for k in keys:
            # print(k)
            types += variant['file_legend']['fields'][k]['type']
        read_size = struct.calcsize(types)
        # logger.debug('file legend types: %s -> %d' % (types, read_size))

        return read_size

    def get_file_legend(self, idx: int) -> dict:
        if self._write_only:
            raise RuntimeError('Write-only Mode')

        if 0 <= idx >= self._h.d['fields']['nr_of_file_legends']:
            raise RuntimeError('Datagram index is out of valid range: 0 <= %s >= %s'
                               % (idx, self._h.d['fields']['nr_of_file_legends']))

        offset = self._h.d['fields']['header_size'] + self._h.d['fields']['sem_size'] \
                 + self._h.d['fields']['ov_size'] + idx * self._dg_size
        # logger.debug("#%d file legend at offset %d" % (idx, offset))
        self._r.seek(offset, 0)

        values = struct.unpack(self._types, self._r.read(self._dg_size))
        fl = dict()
        fl['fields'] = dict(zip(self._keys, values))

        if 'path' in fl['fields']:
            fl['fields']['path'] = fl['fields']['path'].replace(b'\xfe', b'').decode().rstrip('\x00')

        return fl

    def close(self):
        self._r.close()
