import json
import logging
import os
from enum import IntEnum
from typing import Dict, List

from pycrosscheck.common.common import Common

logger = logging.getLogger(__name__)


class SemFormat:
    class Endianess(IntEnum):
        LITTLE = 0
        BIG = 1
        BOTH = 2

    here = os.path.normpath(os.path.dirname(__file__))

    @classmethod
    def list(cls) -> List[str]:
        return Common.files(folder=cls.here, ext='.json')

    @classmethod
    def ext_dict(cls) -> Dict[str, dict]:
        ed = dict()

        for f in cls.list():
            od = cls.load_from_json(json_path=f)
            for ext in od['exts']:
                ed[ext] = od

        return ed

    @classmethod
    def dump_to_json(cls, fmt: dict) -> str:
        json_path = os.path.join(cls.here, '%s.json' % fmt['name'])
        with open(json_path, 'w') as fod:
            json.dump(fmt, fod, indent=4)
        return json_path

    @classmethod
    def load_from_json(cls, json_path: str) -> dict:
        with open(json_path) as fid:
            # noinspection PyTypeChecker
            od = json.load(fid, object_pairs_hook=dict)
        return od
