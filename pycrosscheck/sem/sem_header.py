import logging
import os
import struct

from pycrosscheck.sem.sem_file_legend import SemFileLegend

logger = logging.getLogger(__name__)


class SemHeader:

    def __init__(self, path: str, variant: dict):
        self._write_only = False
        if not os.path.exists(path):
            logger.debug('write-only mode')
            self._write_only = True

        self._p = path
        self._v = variant

        keys = self._v['header']['fields'].keys()
        types = str()
        types += '@'

        for k in keys:
            # print(k)
            types += self._v['header']['fields'][k]['type']
        self.read_size = struct.calcsize(types)
        # logger.debug('types: %s -> %d' % (types, self.read_size))

        self._d = dict()

        if self._write_only:
            return

        self._r = open(self._p, mode='rb')
        # logger.debug('opened in read mode: %s' % os.path.basename(self._p))

        values = struct.unpack(types, self._r.read(self.read_size))
        self._d['fields'] = dict(zip(keys, values))

        self._r.close()

        self._d['fields']['rows'] = int(self._d['fields']['height'] / self._d['fields']['sem_step_size'] + 1)
        self._d['fields']['columns'] = int(self._d['fields']['width'] / self._d['fields']['sem_step_size'] + 1)

        self._d['fields']['file_size'] = os.stat(self._p).st_size
        self._d['fields']['header_size'] = self.read_size
        self._d['fields']['sem_size'] = self._d['fields']['rows'] * self._d['fields']['columns'] * 4
        self._d['fields']['ov_size'] = self._d['fields']['ov_rows'] * self._d['fields']['ov_columns'] * 4
        self._d['fields']['file_legends_size'] = \
            self._d['fields']['nr_of_file_legends'] * SemFileLegend.size(variant=self._v)

    def print_header(self):
        logger.debug("header (%d fields)" % (len(self._d['fields']),))
        for k, v in self._d['fields'].items():
            if isinstance(v, bytes):
                logger.debug(" - %s -> %s" % (k, v.decode()))
            else:
                logger.debug(" - %s -> %s" % (k, v))

    def check_size(self) -> None:

        diff = self._d['fields']['file_size'] - self._d['fields']['header_size'] - self._d['fields']['sem_size'] - \
               self._d['fields']['ov_size'] - self._d['fields']['file_legends_size']
        if diff != 0:
            logger.debug("size check: file_size (%d) - header_size (%d) - sem_size (%d) - ov_size (%d) - "
                         "file_legends_size (%d) = %d"
                         % (self._d['fields']['file_size'], self._d['fields']['header_size'],
                            self._d['fields']['sem_size'],
                            self._d['fields']['ov_size'], self._d['fields']['file_legends_size'], diff))
            raise RuntimeError("Invalid file size: %d" % diff)

    @property
    def d(self) -> dict:
        return self._d

    @property
    def v(self) -> dict:
        return self._v
