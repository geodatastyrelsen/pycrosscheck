from __future__ import annotations

import logging
import os
import struct
from typing import TYPE_CHECKING, Optional

import numpy as np

if TYPE_CHECKING:
    from pycrosscheck.sem.sem_header import SemHeader

logger = logging.getLogger(__name__)


class SemBathy:

    def __init__(self, path: str, variant: dict, header: Optional[SemHeader]):
        self._write_only = False
        if not os.path.exists(path):
            logger.debug('write-only mode')
            self._write_only = True

        self._p = path
        self._v = variant
        self._h = header
        self._b = None  # type: Optional[np.ma.masked_array]

        self._r = open(self._p, mode='rb')
        # logger.debug('opened in read mode: %s' % os.path.basename(self._p))

    @property
    def bathy(self) -> Optional[np.ma.masked_array]:
        return self._b

    def read_bathymetry(self) -> None:
        if self._write_only:
            raise RuntimeError('Write-only Mode')

        offset = self._h.d['fields']['sem_position'] * 4
        logger.debug("bathymetry offset: %d" % offset)

        self._r.seek(offset, 0)
        rows = self._h.d['fields']['rows']
        cols = self._h.d['fields']['columns']
        values = struct.unpack("%di" % (rows * cols), self._r.read(self._h.d['fields']['sem_size']))
        arr = np.array(values).reshape((rows, cols))
        logger.debug(arr)
        arr = np.ma.masked_where(arr == 4095, arr)
        arr = (np.ma.right_shift(arr, 12) - 2048) / 100.0
        self._b = arr
