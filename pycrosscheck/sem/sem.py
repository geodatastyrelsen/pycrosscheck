import logging
import os

import matplotlib
import numpy as np

matplotlib.use('TkAgg')

import matplotlib.pyplot as plt

from typing import Optional, List

from pycrosscheck.sem.sem_format import SemFormat
from pycrosscheck.sem.sem_header import SemHeader
from pycrosscheck.sem.sem_bathy import SemBathy
from pycrosscheck.sem.sem_file_legend import SemFileLegend

logger = logging.getLogger(__name__)


class Sem:

    def __init__(self, path: str, ext: Optional[str] = None):
        self._write_only = False
        if not os.path.exists(path):
            logger.debug('write-only mode')
            self._write_only = True
        self._p = path
        self._ext = ext
        if self._ext is None:
            self._ext = os.path.splitext(self._p)[-1]
            if '.bk' in self._ext:
                self._ext = os.path.splitext(os.path.splitext(self._p)[-2])[-1]
        try:
            self._f = SemFormat.ext_dict()[self._ext]
        except Exception as e:
            raise RuntimeError('Unable to retrieve format: %s' % e)
        try:
            self._v = self._f['variants'][0]
        except Exception as e:
            raise RuntimeError('Unable to retrieve variant: %s' % e)

        self._h = None  # type: Optional[SemHeader]
        self._fl = None  # type: Optional[List[dict]]
        self._b = None  # type: Optional[np.ndarray]

    @property
    def ext(self) -> str:
        return self._ext

    @property
    def header(self) -> Optional[SemHeader]:
        return self._h

    def read_header(self) -> None:
        if self._write_only:
            raise RuntimeError('Write-only Mode')

        self._h = SemHeader(path=self._p, variant=self._v)

    def print_header(self) -> None:
        self._h.print_header()

    def check_header_size(self) -> None:
        self._h.check_size()

    def check_variant(self):
        if self._write_only:
            raise RuntimeError('Write-only Mode')
        self._check_endianess()

    def _check_endianess(self):
        if self._v['endianess']['type'] == 0:
            self._v['endianess']['result'] = SemFormat.Endianess.LITTLE

        elif self._v['endianess']['type'] == 1:
            self._v['endianess']['result'] = SemFormat.Endianess.BIG

        elif self._v['endianess']['type'] == 2:
            raise RuntimeError('Missing a method to detect endianess is format that supports both ones')

        else:
            raise RuntimeError("Unsupported type: %s" % self._v['endianess']['type'])

    @property
    def file_legends(self) -> Optional[List[dict]]:
        return self._fl

    def read_file_legends(self) -> None:
        if self._write_only:
            raise RuntimeError('Write-only Mode')

        self._fl = list()
        sfl = SemFileLegend(path=self._p, variant=self._v, header=self._h)
        for i in range(self._h.d['fields']['nr_of_file_legends']):
            fl = sfl.get_file_legend(idx=i)
            self._fl.append(fl)

    def print_file_legends(self) -> None:

        if self._fl is None:
            self.read_file_legends()

        for idx, fl in enumerate(self._fl):
            logger.debug("#%d file legend:" % idx)
            SemFileLegend.print(self._fl[idx])

    def print_file_legend(self, idx: int) -> None:
        logger.debug("#%d file legend:" % idx)
        SemFileLegend.print(self._fl[idx])

    def check_file_legends(self) -> None:

        if self._fl is None:
            self.read_file_legends()

        counter = 0
        for idx, fl in enumerate(self._fl):
            file_path = fl['fields']['path']
            # logger.debug("#%d file path: %s" % (idx, file_path))
            if not os.path.exists(file_path):
                raise RuntimeError("Unable to locate %s" % file_path)
            counter += 1

        logger.debug("All %d paths in file legends located!" % counter)

    def read_bathy(self) -> None:
        if self._write_only:
            raise RuntimeError('Write-only Mode')

        sb = SemBathy(path=self._p, variant=self._v, header=self._h)
        sb.read_bathymetry()
        self._b = sb.bathy

    @property
    def bathy(self) -> Optional[np.ma.masked_array]:
        return self._b

    def show_bathy(self) -> None:
        if self._b is None:
            self.read_bathy()

        extent = (self._h.d["fields"]['org_easting'],
                  self._h.d["fields"]['org_easting'] + self._h.d["fields"]['width'],
                  self._h.d["fields"]['org_northing'],
                  self._h.d["fields"]['org_northing'] + self._h.d["fields"]['height'])
        plt.imshow(self._b, origin='lower', extent=extent)
        plt.ticklabel_format(style='plain')
        locs, labels = plt.xticks()
        plt.setp(labels, rotation=90)
        plt.show()

    def show_bathy_hist(self) -> None:
        if self._b is None:
            self.read_bathy()

        plt.hist(self._b.flatten(), density=False, bins=30, histtype='step')  # density=True for probability
        plt.ylabel('Count')
        plt.xlabel('Depth')
        plt.show()
