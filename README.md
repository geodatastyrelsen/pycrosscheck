# PyCrossCheck #

This repository implements a method to analyze/estimate the sources of error among pairs of intersecting multibeam survey lines. 

The expected input files are in SEM and FAU formats.

## Installation ##

The *oshydro.sussie* package required to read FAU files is available [here](https://bitbucket.org/geodatastyrelsen/sussie/src/master/)

## Documentation ##

The implemented approach is inspired by Eeg, Jørgen, "Multibeam Crosscheck Analysis: A Case Study", International Hydrographic Review, November 2010 (the article is available in the *resources* folder).

For more info, please contact: gimas@gst.dk; ovand@gst.dk; phsic@gst.dk
